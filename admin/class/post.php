<?php
    class Post{
        // declaração de atributos
        private $id_post;
        private $id_categoria;
        private $titulo_post;
        private $descricao_post;
        private $img_post;
        private $visitas;
        private $data_post;
        private $post_ativo;
    // declaração de métodos de acesso (Getters and Setters) 
        public function getId_Post(){
            return $this->id_post;
        }
        public function setId_Post($value){
            $this->id_post = $value;
        }

        public function getId_Categoria(){
            return $this->id_categoria;
        }
        public function setId_Categoria($value){
            $this->id_categoria = $value;
        }

        public function getTitulo_Post(){
            return $this->titulo_post;
        }
        public function setTitulo_Post($value){
            $this->titulo_post = $value;
        }

        public function getDescricao_Post(){
            return $this->descricao_post;
        }
        public function setDescricao_Post($value){
            $this->descricao_post = $value;
        }

        public function getImg_Post(){
            return $this->img_post;
        }
        public function setImg_Post($value){
            $this->img_post = $value;
        }

        public function getVisitas(){
            return $this->visitas;
        }
        public function setVisitas($value){
            $this->visitas = $value;
        }

        public function getData_Post(){
            return $this->data_post;
        }
        public function setData_Post($value){
            $this->data_post = $value;
        }

        public function getPost_Ativo(){
            return $this->post_ativo;
        }
        public function setPost_Ativo($value){
            $this->post_ativo = $value;
        }
        
        public function loadById($_id_post){
            $sql = new Sql();
            $results = $sql->select("SELECT * FROM post WHERE id_post = :id",array(':id'=>$_id_post));
            if (count($results)>0){
                $this->setData($results[0]);
            }
        }

        public static function getList(){
            $sql = new Sql();
            return $sql->select("SELECT * FROM post order by titulo_post");
        }
        public static function search($titulo_post){
            $sql = new Sql();
            return $sql->select("SELECT * FROM post WHERE titulo_post LIKE :titulo",array(":titulo"=>"%".$titulo_post."%"));
        }
        


        public function setData($data){
            $this->setId_Post($data['id_post']);
            $this->setId_Categoria($data['id_categoria']);
            $this->setTitulo_Post($data['titulo_post']);
            $this->setDescricao_Post($data['descricao_post']);
            $this->setImg_Post($data['img_post']);
            $this->setVisitas($data['visitas']);
            $this->setData_Post($data['data_post']);
            $this->setPost_Ativo($data['post_ativo']);
        }

        public function insert($_id_categoria, $_titulo_post, $_descricao_post, $_img_post, $_visitas,$_data_post, $_post_ativo){
            $sql = new Sql();
            $results = $sql->select("CALL sp_post_insert( :id_categoria, :titulo_post, :descricao_post, :img_post, :visitas, :data_post, :post_ativo)", array(
                ":id_categoria"=>$_id_categoria,
                ":titulo_post"=>$_titulo_post,
                ":descricao_post"=>$_descricao_post,
                ":img_post"=>$_img_post,
                ":visitas"=>$_visitas,
                ":data_post"=>$_data_post,
                ":post_ativo"=>$_post_ativo
            ));
            if (count($results)>0){
                $this->setData($results[0]);
            }
        }

        public function updateVisita($_id)
        {
            $sql = new Sql();
            $sql->query('UPDATE post set visitas = visitas + 1 where id_post = :id',array(':id'=>$_id));
        }

        public function update($_id_post, $_id_categoria, $_titulo_post, $_descricao_post, $_img_post, $_visitas, $_data_post, $_post_ativo){
            $sql = new Sql();
            $results = $sql->query("UPDATE post SET id_categoria = :id_categoria, titulo_post = :titulo_post, descricao_post = :descricao_post, img_post = :img_post, visitas = :visitas, data_post = :data_post, post_ativo = :post_ativo WHERE id_post = :id_post", array(
                ":id_post"=>$_id_post,
                ":id_categoria"=>$_id_categoria,
                ":titulo_post"=>$_titulo_post,
                ":descricao_post"=>$_descricao_post,
                ":img_post"=>$_img_post,
                ":visitas"=>$_visitas,
                ":data_post"=>$_data_post,
                ":post_ativo"=>$_post_ativo
            ));
        }
        public function delete(){
            $sql = new Sql();
            $sql->query("DELETE FROM post WHERE id_post = :id_post", array(":i"=>$this->getId_Post()));
        }

        public function __construct($_id_categoria="",$_titulo_post="",$_descricao_post="",$_img_post="",$_visitas="",$_data_post="",$_post_ativo=""){
            $this->id_categoria = $_id_categoria;
            $this->titulo_post = $_titulo_post;
            $this->descricao_post = $_descricao_post;
            $this->img_post = $_img_post;
            $this->visitas = $_visitas;
            $this->data_post = $_data_post;
            $this->post_ativo = $_post_ativo;
        }
    }
?>