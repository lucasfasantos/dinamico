<?php
    class Usuario
    {
        #Definindo atributos
        private $id;
        private $nome;
        private $email;
        private $senha;
        
        #Definindo métodos de acesso aos atributos
        public function setId($value)
        {
            $this->id = $value;
        }
        public function getId()
        {
            return $this->id;
        }
        public function setNome($value)
        {
            $this->nome = $value;
        }
        public function getNome()
        {
            return $this->nome;
        }
        public function setEmail($value)
        {
            $this->email = $value;
        }
        public function getEmail()
        {
            return $this->email;
        }       
        public function setSenha($value)
        {
            $this->senha = $value;
        }
        public function getSenha()
        {
            return $this->senha;
        }
        #Métodos

        #Inserindo Usuario
        public function insertUser($_nome,$_email,$_senha)
        {
            $sql = new Sql();
            $result = $sql->select('call sp_usuario_insert (:nome,:email,:senha)',array(':nome'=>$_nome,':email'=>$_email,':senha'=>$_senha));
            if(count($result)>0)
            {
                $this->setData($result[0]);
            }
        }
        #Alterar Usuario
        public function updateUser($_id,$_nome,$_email,$_foto)
        {
            $sql = new Sql();
            $sql->query('UPDATE usuario SET nome = :nome, email = :email WHERE id = :id',
            array(  ':nome'=>$_nome,
                    ':email'=>$_email,
                    ':id'=>$_id));
        }
        #Excluir Usuario
        public function deleteUser($_id)
        {
            $sql = new Sql();
            $sql->query('delete from usuario where id = :id',array(':id'=>$_id));
        }
        #Listando usuarios
        public function listarUsers()
        {
            $sql = new Sql();
            return $sql->select('select * from usuario');            
        }
        #Consultar usuario pelo id
        public function consultarUserId($_id)
        {
            $sql = new Sql();
            return $sql->select('select * from usuario where id = :id',array(':id'=>$_id));   
        }
        #Logando usuario        
        public function logarUser($_email,$_senha)
        {
            $sql = new Sql();
            $results = $sql->select('select * from usuario where email = :email AND senha = :senha',array(':email' => $_email,':senha' => $_senha));
            if(count($results)>0)
            {
                $this->setData($results[0]);
            }
        }
        #Definindo dados do banco de dados aos atributos
        public function setData($dados)
        {
            $this->id = $dados['id'];
            $this->nome = $dados['nome'];
            $this->email = $dados['email'];
            $this->senha = $dados['senha'];
        }
        #Método construtor
        public function __construct($_id='',$_nome='',$_email='',$_senha='')
        {
            $this->id = $_id;
            $this->nome = $_nome;
            $this->email = $_email;
            $this->senha = $_senha;
        }
    }
?>