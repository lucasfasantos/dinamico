<?php
    class Banner{
        // declaração de atributos
        private $id_banner;
        private $titulo_banner;
        private $link_banner;
        private $img_banner;
        private $alt;
        private $banner_ativo;
    // declaração de métodos de acesso (Getters and Setters) 
        public function getId_Banner(){
            return $this->id_banner;
        }
        public function setId_Banner($value){
            $this->id_banner = $value;
        }

        public function getTitulo_Banner(){
            return $this->titulo_banner;
        }
        public function setTitulo_Banner($value){
            $this->titulo_banner = $value;
        }

        public function getLink_Banner(){
            return $this->link_banner;
        }
        public function setLink_Banner($value){
            $this->link_banner = $value;
        }

        public function getImg_Banner(){
            return $this->img_banner;
        }
        public function setImg_Banner($value){
            $this->img_banner = $value;
        }

        public function getAlt(){
            return $this->alt;
        }
        public function setAlt($value){
            $this->alt = $value;
        }
        
        public function getBanner_Ativo(){
            return $this->banner_ativo;
        }
        public function setBanner_Ativo($value){
            $this->banner_ativo = $value;
        }
        
        public function loadById($_id_banner){
            $sql = new Sql();
            $results = $sql->select("SELECT * FROM banner WHERE id_banner = :id",array(':id'=>$_id_banner));
            if (count($results)>0){
                $this->setData($results[0]);
            }
        }

        public static function getList(){
            $sql = new Sql();
            return $sql->select("SELECT * FROM banner order by titulo_banner");
        }
        public static function search($titulo_banner){
            $sql = new Sql();
            return $sql->select("SELECT * FROM banner WHERE titulo_banner LIKE :titulo",array(":titulo"=>"%".$titulo_banner."%"));
        }

        public function setData($data){
            $this->setId_Banner($data['id_banner']);
            $this->setTitulo_Banner($data['titulo_banner']);
            $this->setLink_Banner($data['link_banner']);
            $this->setImg_Banner($data['img_banner']);
            $this->setAlt($data['alt']);
            $this->setBanner_Ativo($data['banner_ativo']);
        }

        public function insert(){
            $sql = new Sql();
            $results = $sql->select("CALL sp_banner_insert(:titulo_banner, :link_banner, :img_banner, :alt, :banner_ativo)", array(
                ":titulo_banner"=>$this->getTitulo_Banner(),
                ":link_banner"=>$this->getLink_Banner(),
                ":img_banner"=>$this->getImg_Banner(),
                ":alt"=>$this->getAlt(),
                ":banner_ativo"=>$this->getBanner_Ativo()
            ));
            if (count($results)>0){
                $this->setData($results[0]);
            }
        }

        public function update($_id_banner, $_titulo_banner, $_link_banner, $_img_banner, $_alt, $_banner_ativo){
            $sql = new Sql();
            $results = $sql->query("UPDATE banner SET titulo_banner = :titulo_banner, link_banner = :link_banner, img_banner = :img_banner, alt = :alt, banner_ativo = :banner_ativo  WHERE id_banner = :id_banner", array(
                ":id_banner"=>$_id_banner,
                ":titulo_banner"=>$_titulo_banner,
                ":link_banner"=>$_link_banner,
                ":img_banner"=>$_img_banner,
                ":alt"=>$_alt,
                ":banner_ativo"=>$_banner_ativo
            ));
        }
        public function delete(){
            $sql = new Sql();
            $sql->query("DELETE FROM banner WHERE id_banner = :id", array(":i"=>$this->getId_Banner()));
        }

        public function __construct($_titulo_banner="",$_link_banner="",$_img_banner="",$_alt="",$_banner_ativo=""){
            $this->titulo_banner = $_titulo_banner;
            $this->link_banner = $_link_banner;
            $this->img_banner = $_img_banner;
            $this->alt = $_alt;
            $this->banner_ativo = $_banner_ativo;
        }
    }
?>