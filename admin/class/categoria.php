<?php
    class Categoria{
        // declaração de atributos
        private $id_categoria;
        private $categoria;
        private $cat_ativo;

    // declaração de métodos de acesso (Getters and Setters) 
        public function getId_categoria(){
            return $this->id_categoria;
        }
        public function setId_categoria($value){
            $this->id_categoria = $value;
        }

        public function getCategoria(){
            return $this->categoria;
        }
        public function setCategoria($value){
            $this->categoria = $value;
        }

        public function getCat_ativo(){
            return $this->cat_ativo;
        }
        public function setCat_ativo($value){
            $this->cat_ativo = $value;
        }
        
        public function loadById($_idcategoria){
            $sql = new Sql();
            $results = $sql->select("SELECT * FROM categoria WHERE id_categoria = :id",array(':id'=>$_idcategoria));
            if (count($results)>0){
                $this->setData($results[0]);
            }
        }

        public static function getList(){
            $sql = new Sql();
            return $sql->select("SELECT * FROM categoria order by categoria");
        }
        public static function search($categoria){
            $sql = new Sql();
            return $sql->select("SELECT * FROM categoria WHERE categoria LIKE :categoria",array(":categoria"=>"%".$categoria."%"));
        }

        public function setData($data){
            $this->setId_categoria($data['id_categoria']);
            $this->setCategoria($data['categoria']);
            $this->setCat_ativo($data['cat_ativo']);
        }

        public function insert($_categoria, $_cat_ativo){
            $sql = new Sql();
            $results = $sql->select("CALL sp_categoria_insert (:categoria, :cat_ativo)", array(
                ":categoria"=>$_categoria,
                ":cat_ativo"=>$_cat_ativo
            ));
            if (count($results)>0){
                $this->setData($results[0]);
            }
        }

        public function update($_idcategoria, $_categoria, $_cat_ativo){
            $sql = new Sql();
            $sql->query("UPDATE categoria SET categoria = :cat, cat_ativo = :cat_ativo WHERE id_categoria = :id", array(
                ":id"=>$_idcategoria,
                ":cat"=>$_categoria,
                ":cat_ativo"=>$_cat_ativo
            ));
        }
        public function delete($_id){
            $sql = new Sql();
            $sql->query("DELETE FROM categoria WHERE id_categoria = :id", array(":id"=>$_id));
        }

        public function __construct($_categoria="",$_cat_ativo=""){
            $this->categoria = $_categoria;
            $this->cat_ativo = $_cat_ativo;
        }
    }
?>