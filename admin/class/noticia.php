<?php
    class Noticia{
        // declaração de atributos
        private $id_noticia;
        private $id_categoria;
        private $titulo_noticia;
        private $img_noticia;
        private $visita_noticia;
        private $data_noticia;
        private $noticia_ativo;
        private $noticia;
    // declaração de métodos de acesso (Getters and Setters) 
        public function getId_Noticia(){
            return $this->id_noticia;
        }
        public function setId_Noticia($value){
            $this->id_noticia = $value;
        }

        public function getId_Categoria(){
            return $this->id_categoria;
        }
        public function setId_Categoria($value){
            $this->id_categoria = $value;
        }

        public function getTitulo_Noticia(){
            return $this->titulo_noticia;
        }
        public function setTitulo_Noticia($value){
            $this->titulo_noticia = $value;
        }

        public function getImg_Noticia(){
            return $this->img_noticia;
        }
        public function setImg_Noticia($value){
            $this->img_noticia = $value;
        }

        public function getVisita_Noticia(){
            return $this->visita_noticia;
        }
        public function setVisita_Noticia($value){
            $this->visita_noticia = $value;
        }

        public function getData_Noticia(){
            return $this->data_noticia;
        }
        public function setData_Noticia($value){
            $this->data_noticia = $value;
        }

        public function getNoticia_Ativo(){
            return $this->noticia_ativo;
        }
        public function setNoticia_Ativo($value){
            $this->noticia_ativo = $value;
        }

        public function getNoticia(){
            return $this->noticia;
        }
        public function setNoticia($value){
            $this->noticia = $value;
        }
        
        public function loadById($_id_noticia){
            $sql = new Sql();
            $results = $sql->select("SELECT * FROM noticias WHERE id_noticia = :id",array(':id'=>$_id_noticia));
            if (count($results)>0){
                $this->setData($results[0]);
            }
        }

        public static function getList(){
            $sql = new Sql();
            return $sql->select("SELECT * FROM noticias order by titulo_noticia");
        }
        public static function search($titulo_noticia){
            $sql = new Sql();
            return $sql->select("SELECT * FROM noticias WHERE titulo_noticia LIKE :titulo_noticia",array(":titulo_noticia"=>"%".$titulo_noticia."%"));
        }

        public static function consultarId($_id)
        {
            $sql = new Sql();
            $a = $sql->select('select * from noticias where id_noticia = :id',array(':id'=>$_id));
            return $a[0];
        }

        public function updateVisita($_id)
        {
            $sql = new Sql();
            $sql->query('update noticias set visita_noticia = visita_noticia + 1 where id_noticia = :id',array(':id'=>$_id));
        }


        public function setData($data){
            $this->setId_Noticia($data['id_noticia']);
            $this->setId_Categoria($data['id_categoria']);
            $this->setTitulo_Noticia($data['titulo_noticia']);
            $this->setImg_Noticia($data['img_noticia']);
            $this->setVisita_Noticia($data['visita_noticia']);
            $this->setData_Noticia($data['data_noticia']);
            $this->setNoticia_Ativo($data['noticia_ativo']);
            $this->setNoticia($data['noticia']);
        }

        public function insert(){
            $sql = new Sql();
            $results = $sql->select("CALL sp_noticia_insert(:id_categoria, :titulo_noticia, :img_noticia, :visita_noticia, :data_noticia, :noticia_ativo, :noticia)", array(
                ":id_categoria"=>$this->getId_Categoria(),
                ":titulo_noticia"=>$this->getTitulo_Noticia(),
                ":img_noticia"=>$this->getImg_Noticia(),
                ":visita_noticia"=>0,
                ":data_noticia"=>$this->getData_Noticia(),
                ":noticia_ativo"=>$this->getNoticia_Ativo(),
                ":noticia"=>$this->getNoticia()
                
            ));
            if (count($results)>0){
                $this->setData($results[0]);
            }
        }

        public function update($_id_noticia, $_id_categoria, $_titulo_noticia, $_img_noticia, $_visita_noticia, $_data_noticia, $_noticia_ativo, $_noticia){
            $sql = new Sql();
            $results = $sql->query("UPDATE noticias SET  id_categoria = :id_categoria, titulo_noticia = :titulo_noticia, img_noticia = :img_noticia, visita_noticia = :visita_noticia, data_noticia = :data_noticia, noticia_ativo = :noticia_ativo, noticia = :noticia  WHERE id_noticia = :id_noticia", array(
                ":id_noticia"=>$_id_noticia,
                ":id_categoria"=>$_id_categoria,
                ":titulo_noticia"=>$_titulo_noticia,
                ":img_visita"=>$_img_noticia,
                ":visita_noticia"=>$_visita_noticia,
                ":data_noticia"=>$_data_noticia,
                ":noticia_ativo"=>$_noticia_ativo,
                ":noticia"=>$_noticia
            ));
        }
        public function delete(){
            $sql = new Sql();
            $sql->query("DELETE FROM noticias WHERE id_noticia = :id_noticia", array(":i"=>$this->getId_Noticia()));
        }

        public function __construct($_id_categoria="",$_titulo_noticia="",$_img_noticia="",$_visita_noticia="",$_data_noticia="",$_noticia_ativo="",$_noticia=""){
            $this->id_categoria = $_id_categoria;
            $this->titulo_noticia = $_titulo_noticia;
            $this->img_noticia = $_img_noticia;
            $this->visita_noticia = $_visita_noticia;
            $this->data_noticia = $_data_noticia;
            $this->noticia_ativo = $_noticia_ativo;
            $this->noticia = $_noticia;
        }
    }
?>