<?php
if (isset($_POST['btn_cadastrar'])){
    require_once('conexao.php');
    $banner = $_POST['txt_banner'];
    $link_banner = $_POST['txt_link_banner'];
    $img_banner = $_POST['txt_img_banner'];
    $alt = $_POST['txt_alt_banner'];
    $ativo = isset($_POST['check_ativo'])? '1':'0';
    $cmd = $cn->prepare("INSERT INTO banner (titulo_banner, link_banner, img_banner, alt, banner_ativo) VALUES (:tit, :link, :img, :alt, :ativ)");
    $cmd->execute(array(
        ':tit'=>$banner,
        ':link'=>$link_banner,
        ':img'=>$img_banner,
        ':alt'=>$alt,
        ':ativ'=>$ativo
    ));
    header('location:principal.php?link=8&msg=ok');
}
?>