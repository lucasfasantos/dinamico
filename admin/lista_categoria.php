<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>lista de categoria</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <table id="tb_categorias" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#fcfcfc">
        <tr bgcolor="#993300" align="center">
            <th width="15%" height="2"> <font size="2" color="#fff">Código</font></th>
            <th width="60%" height="2"> <font size="2" color="#fff">Categoria</font></th>
            <th width="15%" height="2"> <font size="2" color="#fff">Ativo</font></th>
            <th colspan="2" ><font size="2" color="#fff">Opções</font></th>
        </tr>
        <?php
        require_once('../config.php');
        $cats = Categoria::getList();
        foreach($cats as $cat){
        ?>
        <tr>
           <td><font size="2" face="verdana, arial" color="black">
               <?php echo $cat['id_categoria']; ?></font></td> 
           <td><font size="2" face="verdana, arial" color="black">
               <?php echo $cat['categoria']; ?></font></td> 
           <td><font size="2" face="verdana, arial" color="black">
               <?php echo $cat['cat_ativo']=='1'?'Sim':'Não'; ?></font></td> 
           <td align="center"><font size="2" face="verdana, arial" color="#fff">
               <a href="<?php echo "alterar_categoria.php?id_categoria=".$cat['id_categoria']."&categoria=".$cat['categoria']."&cat_ativo=".$cat['cat_ativo']; ?>">Alterar</a>
            </font></td>
           <td align="center"><font size="2" face="verdana, arial" color="#fff">
               <a href="<?php echo "op_categoria.php?excluir=1&id=".$cat['id_categoria']; ?>">excluir</a>
                </font></td>
        </tr>
        <?php } ?>
    </table>
    
</body>
</html>