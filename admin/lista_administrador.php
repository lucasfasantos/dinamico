<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php
    echo isset($_GET['msg'])? $_GET['msg']:"";
    ?>
    <title>Lista Administrador</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <table id="tb_administradores" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#fcfcfc">
        <tr bgcolor="#993300" align="center">
            <th width="15%" height="2"><font size="2" color="#fff">ID</font></th>
            <th width="60%" height="2"><font size="2" color="#fff">Nome</font></th>
            <th width="15%" height="2"><font size="2" color="#fff">Email</font></th>
            <th width="15%" height="2"><font size="2" color="#fff">login</font></th>
            <th colspan="2"><font size="2" color="#fff">Opções</font></th>
        </tr>
        <?php 
            require_once('../config.php');
            $admins = Administrador::getList();
            foreach($admins as $adm){
        ?>
        <tr>
            <td><font size="2" face="verdana, arial" color="#0cc">
                <?php echo $adm['id']; ?></font></td>
            <td><font size="2" face="verdana, arial" color="#cc0">
                <?php echo  $adm['nome']; ?></font></td>
            <td><font size="2" face="verdana, arial" color="#c0c">
                <?php echo  $adm['email']; ?></font></td>
            <td><font size="2" face="verdana, arial" color="#c0c">
                <?php echo  $adm['login']; ?></font></td>
            <td align="center"><font size="2" face="verdana, arial" color="#fff">
                <a href="<?php echo "alterar_administrador.php?id=".$adm['id']."&nome=".$adm['nome']."&email=".$adm['email']."&login=".$adm['login']; ?>">Alterar</a>
            </font></td>
            <td align="center"><font size="2" face="verdana, arial" color="#fff">
                <a href="<?php echo "op_administrador.php?excluir=1&id=".$adm['id']; ?>">Excluir</a>
            </font></td>
        </tr>
<?php } ?>
    </table>
    
</body>
</html>



<!-- <4?php
require_once('conexao.php');
$query = "select * from administrador";
$cmd = $cn->prepare($query);
$cmd->execute();
$administradores_retornados = $cmd->fetchAll(PDO::FETCH_ASSOC);
if(count($administradores_retornados)>0){


?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>lista de administrador</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <table id="tb_administrador" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#fcfcfc">
        <tr bgcolor="#993300" align="center">
            <th width="10%" height="2"> <font size="2" color="#fff">Código</font></th>
            <th width="20%" height="2"> <font size="2" color="#fff">Nome</font></th>
            <th width="30%" height="2"> <font size="2" color="#fff">Email</font></th>
            <th width="10%" height="2"> <font size="2" color="#fff">Login</font></th>
            <th width="20%" height="2"> <font size="2" color="#fff">Senha</font></th>
            <th colspan="2"><font size="2" color="#fff">Opções</font></th>
        </tr>
        <4?php
        foreach($administradores_retornados as $administrador){
        ?>
        <tr>
            <td><font size="2" face="verdana, arial" color="black"><4?php echo $administrador['id']?></font></td>
            <td><font size="2" face="verdana, arial" color="black"><4?php echo $administrador['nome']?></font></td>
            <td><font size="2" face="verdana, arial" color="black"><4?php echo $administrador['email']?></font></td>
            <td><font size="2" face="verdana, arial" color="black"><4?php echo $administrador['login']?></font></td>
            <td><font size="2" face="verdana, arial" color="black"><4?php echo $administrador['senha']?></font></td>
            <td align="center"><font size="2" face="verdana, arial" color="#fff"><a href="principal.php?link=">Alterar</a></font></td>
            <td align="center"><font size="2" face="verdana, arial" color="#fff"><a href="principal.php?link=">Excluir</a></font></td>
        </tr>
        <4?php }} ?>
    </table>
    
</body>
</html> -->