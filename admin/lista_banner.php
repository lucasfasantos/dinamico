<?php
require_once('conexao.php');
$query = "select * from banner";
$cmd = $cn->prepare($query); 
$cmd->execute();
$banners_retornados = $cmd->fetchAll(PDO::FETCH_ASSOC);
if(count($banners_retornados)>0){


?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>lista de banner</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <table id="tb_banner" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#fcfcfc">
        <tr bgcolor="#993300" align="center">
            <th width="10%" height="2"> <font size="2" color="#fff">Código</font></th>
            <th width="20%" height="2"> <font size="2" color="#fff">Titulo Banner</font></th>
            <th width="20%" height="2"> <font size="2" color="#fff">Link Banner</font></th>
            <th width="20%" height="2"> <font size="2" color="#fff">Img Banner</font></th>
            <th width="10%" height="2"> <font size="2" color="#fff">Alt</font></th>
            <th width="10%" height="2"> <font size="2" color="#fff">Banner Ativo</font></th>
            <th colspan="2"><font size="2" color="#fff">Opções</font></th>
        </tr>
        <?php
        foreach($banners_retornados as $banner){
        ?>
        <tr>
            <td><font size="2" face="verdana, arial" color="black"><?php echo $banner['id_banner']?></font></td>
            <td><font size="2" face="verdana, arial" color="black"><?php echo $banner['titulo_banner']?></font></td>
            <td><font size="2" face="verdana, arial" color="black"><?php echo $banner['link_banner']?></font></td>
            <td><font size="2" face="verdana, arial" color="black"><?php echo $banner['img_banner']?></font></td>
            <td><font size="2" face="verdana, arial" color="black"><?php echo $banner['alt']?></font></td>
            <td><font size="2" face="verdana, arial" color="black"><?php echo $banner['banner_ativo']=='1'?'Sim':'Não'; ?></font></td>
            <td align="center"><font size="2" face="verdana, arial" color="#fff"><a href="principal.php?link=">alterar</a></font></td>
            <td align="center"><font size="2" face="verdana, arial" color="#fff"><a href="principal.php?link=">excluir</a></font></td>
        </tr>
        <?php }} ?>
    </table>    

</body>
</html>