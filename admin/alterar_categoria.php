<?php
$id = filter_input(INPUT_GET,'id_categoria');
$categoria = filter_input(INPUT_GET,'categoria');
$cat_ativo = filter_input(INPUT_GET,'cat_ativo');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Alteração de categoria</title>
</head>
<body>
    <form action="op_categoria.php?alterar=1" method="POST" enctype="multipart/form-data">
    <fieldset>
        <legend>Alteração de Categoria</legend>
        <div>
            <input type="hidden" name="id" value="<?php echo $id?>">
        </div>
        <div>
            <input type="text" name="categoria" value="<?php echo $categoria?>">
        </div>
        <div>
            <input type="checkbox" name="check_ativo" <?php echo $cat_ativo==1?'checked':''?>>
        </div>
        <div>
            <input type="submit" name="alterar" value="Registrar Alteração">
        </div>
    </fieldset>
    
    </form>
</body>
</html>