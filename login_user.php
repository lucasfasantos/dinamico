<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Site Dinâmico - Área de login</title>
    <link rel="stylesheet" href="admin/css/style.css">
</head>
<body>
    <div  id="box-login">
        <div id="formulario-menor">
            <form id="frmlogin" name="frmlogin" action="op_login_user.php" method="POST">
                <fieldset>
                    <legend>Área de login</legend>
                    <label for=""><span>Email</span></label>
                    <input type="text" name="txt_email" id="txt_email">

                    <label for=""><span>Senha</span></label>
                    <input type="password" name="txt_senha" id="txt_senha">

                    <input type="submit" name="logar" id="logar" value="Logar" class="botao">
                    

                    <input type="submit" name="cadastrar" id="cadastrar" value="Cadastrar-se" class="botao">
                    <span><?php echo (isset($_GET['msg']))?$_GET['msg']:"";?></span>
                </fieldset>
            </form>
        </div>
    </div>
    
</body>
</html>