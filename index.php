<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Site Dinâmico UC12</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>  
    <div id="estrutura">
        <div id="topo"></div>
        <div id="menu">
            <ul>
                <li><a href="index.php?link=1">Home</a></li>
                <li><a href="index.php?link=">Serviços</a></li>
                <li><a href="index.php?link=">Produtos</a></li>
                <li><a href="index.php?link=">Contatos</a></li>
            </ul>        
        </div>
        <div id="banner" class="banner"></div>

        <div id="corpo">
            <div id="esquerda" class="esquerda">
                <h1>Produtos</h1>
                <li><a href="index.php?link=5">Produto 1</a></li>
                <li><a href="index.php?link=">Produto 2</a></li>
                <li><a href="index.php?link=">Produto 3</a></li>
                <li><a href="index.php?link=">Produto 4</a></li>
                <br>
                <br>

                
                
                
                <?php
                require_once('op_login_user.php');
                if ($_SESSION['logado'] == false)
                {
                    echo '<h1>Login ou Cadastrar</h1>
                    <li><a href="index.php?link=15">Login ou Cadastrar</a></li>
                    <br>';
                }

                
                if ($_SESSION['logado'] == true )
                {
                    echo '(<a href="op_login_user.php?sair=1">Sair</a>)';
                }
                ?>
                
            </div>
            <div id="centro">
                <?php
                   $link = (isset($_GET['link'])?$_GET['link']:'');
                   $_SESSION['id_noticia']= filter_input(INPUT_GET,'id_noticia');
                   $_SESSION['id_post']= filter_input(INPUT_GET,'id_post');
                   $pag[1] = "home.php";
                   $pag[15] = "login_user.php";
                   $pag[5] = "produto.php";
                   $pag[3] = "conteudo_noticia.php";
                   $pag[4] = "conteudo_post.php";
                   $pag[2] = "cadastrar_user.php";
                   if (!empty($link)){
                       if(file_exists($pag[$link])){
                            include($pag[$link]);
                       }
                       else{
                            include($pag[1]); // mostre o home
                       }
                    }
                    else{
                        include($pag[1]); // mostre o home
                    }

                ?>
            </div>
            <div id="direita">

                    <h1>Noticias</h1>

                <div id="noticias">                     
                    <?php
                        require_once('config.php');
                        $noticias = Noticia::getList();
                        foreach ($noticias as $noticia)
                        {   
                            if($noticia['noticia_ativo']=='1')
                            {
                    ?>   
                    <h3><img src="admin/foto/<?php echo $noticia['img_noticia']; ?>" alt="" width="90" height="80"></h3>
                    <div id="itens-noticias">
                        <span><?php echo $noticia['data_noticia']; ?></span>
                        <a href="index.php?link=3&id_noticia=<?php echo $noticia['id_noticia']; ?>"><?php echo $noticia['titulo_noticia']; ?></a>
                    </div>

                    <?php } }?>                    
                </div>
                <br>
                <br>
                <br>
                
                <h1>Post</h1>

                <div id="post">                     
                    <?php
                        require_once('config.php');
                        $posts = Post::getList();
                        foreach ($posts as $post)
                        {   
                            if($post['post_ativo']=='1')
                            {
                    ?>   
                    <h3><img src="admin/foto/<?php echo $post['img_post']; ?>" alt="" width="90" height="80"></h3>
                    <div id="itens-post">
                        <span><?php echo $post['data_post']; ?></span>
                        <a href="index.php?link=4&id_post=<?php echo $post['id_post']; ?>"><?php echo $post['titulo_post']; ?></a>
                    </div>
                    <?php } } ?>                    
                </div> 
            </div>
            <div> 
                    Área do Administrador
                    <a href="admin/index.php">Acesso à área Administrativa</a>
                </div>
                <div id="rodape">
                    &copy; - Todos os direitos reservados
                </div>
            </div>
        </div>


    </div>
</body>
</html>