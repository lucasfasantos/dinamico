<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cadastrar-se</title>
    <link rel="stylesheet" href="admin/css/style.css">
</head>
<body>
    <div id="box-cadastrar">
        <div id="formulario-menor">
            <form id="frmcadastrar" name="frmcadastrar-se" action="op_cadastrar_user.php" method="POST">
                <fieldset>
                    <legend>Área de Cadastro</legend>
                    <label for=""><span>Nome Completo</span></label>
                    <input type="text" name="txt_nome" id="txt_nome">

                    <label for=""><span>Email</span></label>
                    <input type="text" name="txt_email" id="txt_email">

                    <label for=""><span>Senha</span></label>
                    <input type="text" name="txt_senha" id="txt_senha">
                    <br>
                    <br>
                    <input type="submit" name="cadastrar-se" id="cadastrar-se" value="Cadastrar" class="btn btn-cadastrar">

                    <span><?php echo (isset($GET['msg']))?$_GET['msg']:"";?></span>
                </fieldset>
            </form>
        </div>
    </div>
    
</body>
</html>